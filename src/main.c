/*
 * Copyright (c) 2020 Nordic Semiconductor ASA
 *
 * SPDX-License-Identifier: LicenseRef-BSD-5-Clause-Nordic
 */

/** @file
 *
 * @brief Simple Zigbee light bulb implementation.
 */

#include <stdbool.h>
#include <stdint.h>

#include <zephyr/types.h>
#include <zephyr.h>
#include <device.h>
#include <devicetree.h>
#include <drivers/sensor.h>
#include <drivers/gpio.h>
#include <drivers/watchdog.h>
#include <ram_pwrdn.h>
#include <logging/log.h>
#include <hal/nrf_power.h>
#include <soc.h>
#include "nrf_802154.h"

#include <nrfx_saadc.h>

#include <zboss_api.h>
#include <zboss_api_addons.h>
#include <zigbee/zigbee_app_utils.h>
#include <zigbee/zigbee_error_handler.h>
#include <zb_nrf_platform.h>
#include "zb_mem_config_custom.h"
#include "zb_multi_sensor.h"

#include "battery.h"


/**
 * Define if watch should be used
 */
#define xUSE_APP_WATCHDOG

/**
 * Min and max of mV for battery
 */
#define MAX_BAT 4200
#define MIN_BAT 3100


#define LED0_NODE DT_ALIAS(status_led)
#define LED0 DT_GPIO_LABEL(LED0_NODE, gpios)
#define LED0_PIN DT_GPIO_PIN(LED0_NODE, gpios)
#define LED0_FLAGS DT_GPIO_FLAGS(LED0_NODE, gpios)


#if CONFIG_ZIGBEE_FOTA
#include <zigbee/zigbee_fota.h>
#include <power/reboot.h>
#include <dfu/mcuboot.h>

/* LED indicating OTA Client Activity. */
//#define OTA_ACTIVITY_LED          DK_LED2
#endif /* CONFIG_ZIGBEE_FOTA */

#define BME280_SLEEP_TIME 60000
#define MAX_ERRORS 3

#define BATTERY_SLEEP_TIME 5 * 60 * 1000
//#define BATTERY_SLEEP_TIME 3000
#define REFV 600  // 600mV
#define RES_BIT 12 // 12 Bit resolution
#define GAIN 1/2 // 1/2 Gain

#define SAADC_CHANNEL_COUNT   1

#define ZIGBEE_TRACE_LEVEL 4
#define ZIGBEE_TRACE_MASK 0xffff

#define STACKSIZE 1024
#define PRIORITY 7

#define BME280 DT_INST(0, bosch_bme280)
#if DT_NODE_HAS_STATUS(BME280, okay)
#define BME280_LABEL DT_LABEL(BME280)
#else
#error Your devicetree has no enabled nodes with compatible "bosch,bme280"
#define BME280_LABEL "<none>"
#endif

//#define ZB_SCHED_SLEEP_THRESHOLD_MS 200

#ifdef IEEE_ADDR
//const static zb_ieee_addr_t g_ed_addr = {0x01, 0x22, 0x22, 0x22, 0x22, 0x22, 0x22, 0x02};
const static zb_ieee_addr_t g_ed_addr = IEEE_ADDR;
#endif
const static struct device *led0;

static int m_isConnected;

K_EVENT_DEFINE(m_bootEvent);
#define EVENT_SENSOR_OKAY 0b0001
#define EVENT_BATTERY_OKAY 0b0010
#define EVENT_ZIGBEE_OKAY 0b0100

LOG_MODULE_REGISTER(app);

static sensor_device_ctx_t m_dev_ctx;
static volatile int m_total_errors;

#ifdef USE_APP_WATCHDOG
static int m_wdtChannel;
const static struct device *wdt;
static struct wdt_timeout_cfg m_wdt_config;
#endif

#define WDT_DEV_NAME DT_LABEL(BME280)

ZB_ZCL_DECLARE_IDENTIFY_ATTRIB_LIST(
	identify_attr_list,
	&m_dev_ctx.identify_attr.identify_time);

ZB_ZCL_DECLARE_BASIC_ATTRIB_LIST_EXT(
	basic_attr_list,
	&m_dev_ctx.basic_attr.zcl_version,
	&m_dev_ctx.basic_attr.app_version,
	&m_dev_ctx.basic_attr.stack_version,
	&m_dev_ctx.basic_attr.hw_version,
	m_dev_ctx.basic_attr.mf_name,
	m_dev_ctx.basic_attr.model_id,
	m_dev_ctx.basic_attr.date_code,
	&m_dev_ctx.basic_attr.power_source,
	m_dev_ctx.basic_attr.location_id,
	&m_dev_ctx.basic_attr.ph_env,
	m_dev_ctx.basic_attr.sw_ver);


ZB_ZCL_DECLARE_TEMP_MEASUREMENT_ATTRIB_LIST(
	temperature_attr_list, 
	&m_dev_ctx.temp_attr.measure_value,
	&m_dev_ctx.temp_attr.min_measure_value, 
	&m_dev_ctx.temp_attr.max_measure_value, 
	&m_dev_ctx.temp_attr.tolerance);

ZB_ZCL_DECLARE_REL_HUMIDITY_MEASUREMENT_ATTRIB_LIST(
	rel_humidity_measurement_attr_list, 
	&m_dev_ctx.rel_hum_attr.measure_value, 
	&m_dev_ctx.rel_hum_attr.min_measure_value, 
	&m_dev_ctx.rel_hum_attr.max_measure_value);

ZB_ZCL_DECLARE_PRESSURE_MEASUREMENT_ATTRIB_LIST(
	pressure_attr_list, 
	&m_dev_ctx.pres_attr.measure_value,
	&m_dev_ctx.pres_attr.min_measure_value, 
	&m_dev_ctx.pres_attr.max_measure_value, 
	&m_dev_ctx.pres_attr.tolerance);

#define bat_num 
ZB_ZCL_DECLARE_POWER_CONFIG_BATTERY_ATTRIB_LIST_EXT(
	power_config_attr_list,
	&m_dev_ctx.power_attr.battery_voltage,
	&m_dev_ctx.power_attr.battery_size,
	&m_dev_ctx.power_attr.battery_quantity,
	&m_dev_ctx.power_attr.battery_rated_voltage,
	&m_dev_ctx.power_attr.battery_alarm_mask,
	&m_dev_ctx.power_attr.battery_voltage_min_threshold,
	&m_dev_ctx.power_attr.battery_percentage_remaining,
	&m_dev_ctx.power_attr.battery_voltage_threshold1,
	&m_dev_ctx.power_attr.battery_voltage_threshold2,
	&m_dev_ctx.power_attr.battery_voltage_threshold3,
	&m_dev_ctx.power_attr.battery_percentage_min_threshold,
	&m_dev_ctx.power_attr.battery_percentage_threshold1,
	&m_dev_ctx.power_attr.battery_percentage_threshold2,
	&m_dev_ctx.power_attr.battery_percentage_threshold3,
	&m_dev_ctx.power_attr.battery_alarm_state);

ZB_DECLARE_MULTI_SENSOR_CLUSTER_LIST(
	multi_sensor_clusters,
	basic_attr_list,
	identify_attr_list,
	temperature_attr_list,
	rel_humidity_measurement_attr_list,
	pressure_attr_list,
	power_config_attr_list);

ZB_ZCL_DECLARE_MULTI_SENSOR_EP(
	multi_sensor_ep,
	MULTI_SENSOR_ENDPOINT,
	multi_sensor_clusters);

#ifndef CONFIG_ZIGBEE_FOTA
ZBOSS_DECLARE_DEVICE_CTX_1_EP(
	multi_sensor_ctx,
	multi_sensor_ep);
#else
extern zb_af_endpoint_desc_t zigbee_fota_client_ep;
ZBOSS_DECLARE_DEVICE_CTX_2_EP(
	multi_sensor_ctx,
	zigbee_fota_client_ep,
	multi_sensor_ep);
#endif /* CONFIG_ZIGBEE_FOTA */



static void sensor_clusters_attr_init(void)
{
	m_dev_ctx.basic_attr.zcl_version   = ZB_ZCL_VERSION;
	m_dev_ctx.basic_attr.app_version   = SENSOR_INIT_BASIC_APP_VERSION;
	m_dev_ctx.basic_attr.stack_version = SENSOR_INIT_BASIC_STACK_VERSION;
	m_dev_ctx.basic_attr.hw_version    = SENSOR_INIT_BASIC_HW_VERSION;

	ZB_ZCL_SET_STRING_VAL(
		m_dev_ctx.basic_attr.mf_name,
		SENSOR_INIT_BASIC_MANUF_NAME,
		ZB_ZCL_STRING_CONST_SIZE(SENSOR_INIT_BASIC_MANUF_NAME));

	ZB_ZCL_SET_STRING_VAL(
		m_dev_ctx.basic_attr.model_id,
		SENSOR_INIT_BASIC_MODEL_ID,
		ZB_ZCL_STRING_CONST_SIZE(SENSOR_INIT_BASIC_MODEL_ID));

	ZB_ZCL_SET_STRING_VAL(
		m_dev_ctx.basic_attr.date_code,
		SENSOR_INIT_BASIC_DATE_CODE,
		ZB_ZCL_STRING_CONST_SIZE(SENSOR_INIT_BASIC_DATE_CODE));

	ZB_ZCL_SET_STRING_VAL(
		m_dev_ctx.basic_attr.sw_ver,
		COMMIT_HASH,
		ZB_ZCL_STRING_CONST_SIZE(COMMIT_HASH));

	m_dev_ctx.basic_attr.power_source = SENSOR_INIT_BASIC_POWER_SOURCE;

	ZB_ZCL_SET_STRING_VAL(
		m_dev_ctx.basic_attr.location_id,
		SENSOR_INIT_BASIC_LOCATION_DESC,
		ZB_ZCL_STRING_CONST_SIZE(SENSOR_INIT_BASIC_LOCATION_DESC));

	m_dev_ctx.basic_attr.ph_env = SENSOR_INIT_BASIC_PH_ENV;

	m_dev_ctx.identify_attr.identify_time = ZB_ZCL_IDENTIFY_IDENTIFY_TIME_DEFAULT_VALUE;

	m_dev_ctx.temp_attr.measure_value = (zb_int16_t)0;
	m_dev_ctx.temp_attr.min_measure_value = (zb_int16_t)-4000;
	m_dev_ctx.temp_attr.max_measure_value = (zb_int16_t)10000;
	m_dev_ctx.temp_attr.tolerance = (zb_int16_t)0;

	m_dev_ctx.rel_hum_attr.measure_value = 0;
	m_dev_ctx.rel_hum_attr.min_measure_value = 0;
	m_dev_ctx.rel_hum_attr.max_measure_value = 10000;

	m_dev_ctx.pres_attr.measure_value = (zb_int16_t)0;
	m_dev_ctx.pres_attr.min_measure_value = (zb_int16_t)2000;
	m_dev_ctx.pres_attr.max_measure_value = (zb_int16_t)15000;
	m_dev_ctx.pres_attr.tolerance = (zb_int16_t)0;

	ZB_ZCL_SET_ATTRIBUTE(
		MULTI_SENSOR_ENDPOINT,
		ZB_ZCL_CLUSTER_ID_REL_HUMIDITY_MEASUREMENT,
		ZB_ZCL_CLUSTER_SERVER_ROLE,
		ZB_ZCL_ATTR_REL_HUMIDITY_MEASUREMENT_VALUE_ID,
		(zb_uint8_t *)&m_dev_ctx.rel_hum_attr.measure_value,
		ZB_FALSE);

	ZB_ZCL_SET_ATTRIBUTE(
		MULTI_SENSOR_ENDPOINT,
		ZB_ZCL_CLUSTER_ID_PRESSURE_MEASUREMENT,
		ZB_ZCL_CLUSTER_SERVER_ROLE,
		ZB_ZCL_ATTR_PRESSURE_MEASUREMENT_VALUE_ID,
		(zb_uint8_t *)&m_dev_ctx.pres_attr.measure_value,
		ZB_FALSE);

	ZB_ZCL_SET_ATTRIBUTE(
		MULTI_SENSOR_ENDPOINT,
		ZB_ZCL_CLUSTER_ID_TEMP_MEASUREMENT,
		ZB_ZCL_CLUSTER_SERVER_ROLE,
		ZB_ZCL_ATTR_TEMP_MEASUREMENT_VALUE_ID,
		(zb_uint8_t *)&m_dev_ctx.temp_attr.measure_value,
		ZB_FALSE);
}



/*
 * Should init the reporting cluster but stil not working
 */
static void configure_attribute_reporting()
{
	LOG_INF("Config reporting");

	zb_zcl_reporting_info_t temp_rep_info;
	memset(&temp_rep_info, 0, sizeof(temp_rep_info));

	temp_rep_info.direction = ZB_ZCL_CONFIGURE_REPORTING_SEND_REPORT;
	temp_rep_info.ep = MULTI_SENSOR_ENDPOINT;
	temp_rep_info.cluster_id = ZB_ZCL_CLUSTER_ID_TEMP_MEASUREMENT;
	temp_rep_info.cluster_role = ZB_ZCL_CLUSTER_SERVER_ROLE;
	temp_rep_info.attr_id = ZB_ZCL_ATTR_TEMP_MEASUREMENT_VALUE_ID;
	temp_rep_info.dst.profile_id = ZB_AF_HA_PROFILE_ID;
	temp_rep_info.u.send_info.min_interval = 30;      // 30 seconds
	temp_rep_info.u.send_info.max_interval = 300;    // 5 minutes
	temp_rep_info.u.send_info.delta.s16 = 0x0001;        // 0.5 degrees



	zb_zcl_reporting_info_t humid_rep_info;
	memset(&humid_rep_info, 0, sizeof(humid_rep_info));

	humid_rep_info.direction = ZB_ZCL_CONFIGURE_REPORTING_SEND_REPORT;
	humid_rep_info.ep = MULTI_SENSOR_ENDPOINT;
	humid_rep_info.cluster_id = ZB_ZCL_CLUSTER_ID_REL_HUMIDITY_MEASUREMENT;
	humid_rep_info.cluster_role = ZB_ZCL_CLUSTER_SERVER_ROLE;
	humid_rep_info.attr_id = ZB_ZCL_ATTR_REL_HUMIDITY_MEASUREMENT_VALUE_ID;
	humid_rep_info.dst.profile_id = ZB_AF_HA_PROFILE_ID;
	humid_rep_info.u.send_info.min_interval = 30;       // 60 seconds
	humid_rep_info.u.send_info.max_interval = 300;     // 10 minutes
	humid_rep_info.u.send_info.delta.u16 = 0x0001;         // 1% rh



	zb_zcl_reporting_info_t press_rep_info;
	memset(&press_rep_info, 0, sizeof(press_rep_info));

	press_rep_info.direction = ZB_ZCL_CONFIGURE_REPORTING_SEND_REPORT;
	press_rep_info.ep = MULTI_SENSOR_ENDPOINT;
	press_rep_info.cluster_id = ZB_ZCL_CLUSTER_ID_PRESSURE_MEASUREMENT;
	press_rep_info.cluster_role = ZB_ZCL_CLUSTER_SERVER_ROLE;
	press_rep_info.attr_id = ZB_ZCL_ATTR_PRESSURE_MEASUREMENT_VALUE_ID;
	press_rep_info.dst.profile_id = ZB_AF_HA_PROFILE_ID;
	press_rep_info.u.send_info.min_interval = 30;       // 30 seconds
	press_rep_info.u.send_info.max_interval = 900;     // 15 minutes
	press_rep_info.u.send_info.delta.u16 = 0x0001;         // 1% rh



	// Prior to SDK 3.1 this function was zb_zcl_put_default_reporting_info(zb_zcl_reporting_info_t*)
	zb_zcl_put_reporting_info(&temp_rep_info, ZB_TRUE);    // Assume the override parameter here is to override the new default configuration.
	zb_zcl_put_reporting_info(&humid_rep_info, ZB_TRUE);
	zb_zcl_put_reporting_info(&press_rep_info, ZB_TRUE);
}



#ifdef USE_APP_WATCHDOG
void initWatchdog()
{
	LOG_INF("Starting watchdog");

	wdt = device_get_binding(WDT_DEV_NAME);
	if(!wdt) {
		LOG_ERR("Could not get watchdog device");
		return;
	}

	m_wdt_config.callback = NULL;
	m_wdt_config.flags = WDT_FLAG_RESET_SOC;
	m_wdt_config.window.min = 0;
	m_wdt_config.window.max = 120 * 1000;

	m_wdtChannel = wdt_install_timeout(wdt, &m_wdt_config);
	if(m_wdtChannel == -ENOTSUP) {
		LOG_ERR("Watchdog timeout config not suppoted");
	}
	if(m_wdtChannel < 0) {
		LOG_ERR("Watchdog install error");
		return;
	}
}

void startWatchdog()
{
	int error_code = wdt_setup(wdt, 0);
	if(error_code < 0) {
		LOG_ERR("Watchdog setup error");
		return;
	}
}

void feedWatchdog()
{
	int error = wdt_feed(wdt, m_wdtChannel);
	if(error) {
		LOG_ERR("Error by feeding watchdog");
		return;
	}
}
#endif


void setZigbeeBatteryPercent(int32_t percent)
{
		m_dev_ctx.power_attr.battery_percentage_remaining = (100 - percent) * 2;
		ZB_ZCL_SET_ATTRIBUTE(
			MULTI_SENSOR_ENDPOINT,
			ZB_ZCL_CLUSTER_ID_POWER_CONFIG,
			ZB_ZCL_CLUSTER_SERVER_ROLE,
			ZB_ZCL_ATTR_POWER_CONFIG_BATTERY_PERCENTAGE_REMAINING_ID,
			(zb_uint8_t *)&m_dev_ctx.power_attr.battery_percentage_remaining,
			ZB_TRUE);
}

void setZigbeeBatteryVoltage(int32_t voltage)
{
		m_dev_ctx.power_attr.battery_voltage = voltage / 100;
		ZB_ZCL_SET_ATTRIBUTE(
			MULTI_SENSOR_ENDPOINT,
			ZB_ZCL_CLUSTER_ID_POWER_CONFIG,
			ZB_ZCL_CLUSTER_SERVER_ROLE,
			ZB_ZCL_ATTR_POWER_CONFIG_BATTERY_VOLTAGE_ID,
			(zb_uint8_t *)&m_dev_ctx.power_attr.battery_voltage,
			ZB_TRUE);
}

void setZigbeePressure(float pressure)
{
	m_dev_ctx.pres_attr.measure_value = (zb_int16_t)(pressure * 10.0);
	ZB_ZCL_SET_ATTRIBUTE(
		MULTI_SENSOR_ENDPOINT,
		ZB_ZCL_CLUSTER_ID_PRESSURE_MEASUREMENT,
		ZB_ZCL_CLUSTER_SERVER_ROLE,
		ZB_ZCL_ATTR_PRESSURE_MEASUREMENT_VALUE_ID,
		(zb_uint8_t *)&m_dev_ctx.pres_attr.measure_value,
		ZB_FALSE);
}

void setZigbeeTemp(float temp)
{
	m_dev_ctx.temp_attr.measure_value = (zb_int16_t)(temp * 100.0);
	ZB_ZCL_SET_ATTRIBUTE(
		MULTI_SENSOR_ENDPOINT,
		ZB_ZCL_CLUSTER_ID_TEMP_MEASUREMENT,
		ZB_ZCL_CLUSTER_SERVER_ROLE,
		ZB_ZCL_ATTR_TEMP_MEASUREMENT_VALUE_ID,
		(zb_uint8_t *)&m_dev_ctx.temp_attr.measure_value,
		ZB_FALSE);
}

void setZigbeeHuminity(float hum)
{
	m_dev_ctx.rel_hum_attr.measure_value = (zb_uint16_t)(hum * 100.0);
	ZB_ZCL_SET_ATTRIBUTE(
		MULTI_SENSOR_ENDPOINT,
		ZB_ZCL_CLUSTER_ID_REL_HUMIDITY_MEASUREMENT,
		ZB_ZCL_CLUSTER_SERVER_ROLE,
		ZB_ZCL_ATTR_REL_HUMIDITY_MEASUREMENT_VALUE_ID,
		(zb_uint8_t *)&m_dev_ctx.rel_hum_attr.measure_value,
		ZB_FALSE);
}

static void battery_workHandler(struct k_work *work)
{
	ARG_UNUSED(work);
	int32_t adc, v;

	k_msleep(2000); // Just wait 2 seconds before go
	LOG_INF("Battery work started");
	battery_init();
	k_msleep(200);

	LOG_INF("Read battery ADC value");
	adc = battery_readAdc();
	v = battery_calcV(adc);

	battery_uninit();

	int32_t range = MAX_BAT - MIN_BAT;
	int32_t rel = MAX_BAT - v;
	int32_t now = 100 * rel / range;

	if(now > 100) now = 100;
	if(now < 0) now = 0;

	LOG_INF("Battery: %d (%dmV - %d)", adc, v, 100 - now);

	setZigbeeBatteryPercent(now);
	setZigbeeBatteryVoltage(v);

	k_event_post(&m_bootEvent, EVENT_BATTERY_OKAY);
}
K_WORK_DEFINE(battery_work, battery_workHandler);

static void bme280_workHandler(struct k_work *work)
{
	ARG_UNUSED(work);
	LOG_INF("BME280 work initialized");

	const struct device *dev = device_get_binding(BME280_LABEL);
	if(dev) {
		LOG_INF("Found device \"%s\"\n", BME280_LABEL);
		struct sensor_value sensor_temp, sensor_press, sensor_humidity;
		float temp, press, hum;

		sensor_sample_fetch(dev);
		sensor_channel_get(dev, SENSOR_CHAN_AMBIENT_TEMP, &sensor_temp);
		sensor_channel_get(dev, SENSOR_CHAN_PRESS, &sensor_press);
		sensor_channel_get(dev, SENSOR_CHAN_HUMIDITY, &sensor_humidity);

		temp = sensor_temp.val1 + (sensor_temp.val2 / 1000000.0);
		press = (sensor_press.val1 + (sensor_press.val2 / 1000000.0)) * 10.0;
		hum = sensor_humidity.val1 + (sensor_humidity.val2 / 1000000.0);
		LOG_INF("Zigbee - temp: %f; press: %f; hum: %f", temp, press, hum);

		setZigbeeTemp(temp);
		setZigbeeHuminity(hum);
		setZigbeePressure(press);
		LOG_INF("Zigbee - send as temp: %d; press: %d; hum: %d", m_dev_ctx.temp_attr.measure_value, m_dev_ctx.pres_attr.measure_value, m_dev_ctx.rel_hum_attr.measure_value);

		k_event_post(&m_bootEvent, EVENT_SENSOR_OKAY);
	} else {
		LOG_ERR("No device \"%s\" found; did initialization fail?\n", BME280_LABEL);
	}

}
K_WORK_DEFINE(bme280_work, bme280_workHandler);




static void bme280_timerHandler(struct k_timer *timer)
{
	ARG_UNUSED(timer);
	k_work_submit(&bme280_work);
}
K_TIMER_DEFINE(bme280_timer, bme280_timerHandler, NULL);

static void battery_timerHandler(struct k_timer *timer)
{
	ARG_UNUSED(timer);
	k_work_submit(&battery_work);
}
K_TIMER_DEFINE(battery_timer, battery_timerHandler, NULL);



void zboss_signal_handler(zb_bufid_t bufid)
{
	zb_zdo_app_signal_hdr_t *sig_hndler = NULL;
	zb_zdo_app_signal_type_t sig = zb_get_app_signal(bufid, &sig_hndler);
	zb_ret_t status = ZB_GET_APP_SIGNAL_STATUS(bufid);

	if(status != 0) {
		if(++m_total_errors > MAX_ERRORS) {
			LOG_WRN("Reboot device, too many errors");
			NVIC_SystemReset();
		}
	}
	LOG_DBG("Status: %d, (%d)", status, sig);

#ifdef CONFIG_ZIGBEE_FOTA
	/* Pass signal to the OTA client implementation. */
	zigbee_fota_signal_handler(bufid);
#endif /* CONFIG_ZIGBEE_FOTA */

#ifdef USE_APP_WATCHDOG
	if(m_isConnected) {
		feedWatchdog();
	}
#endif

	switch(sig) {
		case ZB_BDB_SIGNAL_DEVICE_FIRST_START:
		case ZB_BDB_SIGNAL_DEVICE_REBOOT:
		case ZB_BDB_SIGNAL_STEERING:
			// configure_attribute_reporting();
			LOG_INF("Joined network, reported from callback");
			if(status == RET_OK) {
				m_isConnected = 1;
				k_event_post(&m_bootEvent, EVENT_ZIGBEE_OKAY);
			} else {
				m_isConnected = 0;
			}
			break;
		case ZB_ZDO_SIGNAL_LEAVE:
			m_isConnected = 0;
			break;
		case ZB_BDB_SIGNAL_WWAH_REJOIN_STARTED:
			if(++m_total_errors > MAX_ERRORS) {
				LOG_WRN("Reboot device, too many errors");
				NVIC_SystemReset();
			}
			break;
		case ZB_ZDO_SIGNAL_LEAVE_INDICATION:
			if(++m_total_errors > MAX_ERRORS) {
				LOG_WRN("Reboot device, too many errors");
				NVIC_SystemReset();
			}
			break;
		case ZB_NLME_STATUS_INDICATION:
			if(status!=RET_OK) {
				LOG_WRN("Failed status, reseting");
				NVIC_SystemReset();
			}
		default:
			break;
	}
	ZB_ERROR_CHECK(zigbee_default_signal_handler(bufid));

	if(bufid) {
		zb_buf_free(bufid);
	}
}


/*
static void NotReportedCb(zb_uint8_t ep, zb_uint16_t cluster_id, zb_uint16_t attr_id)
{
	LOG_INF("Attribute not reported %d, %d, %d", ep, cluster_id, attr_id);
}
*/


#ifdef CONFIG_ZIGBEE_FOTA
static void confirm_image(void)
{
	if (!boot_is_img_confirmed()) {
		int ret = boot_write_img_confirmed();

		if (ret) {
			LOG_ERR("Couldn't confirm image: %d", ret);
		} else {
			LOG_INF("Marked image as OK");
		}
	}
}

static void ota_evt_handler(const struct zigbee_fota_evt *evt)
{
	switch (evt->id) {
	case ZIGBEE_FOTA_EVT_PROGRESS:
		//dk_set_led(OTA_ACTIVITY_LED, evt->dl.progress % 2);
		break;

	case ZIGBEE_FOTA_EVT_FINISHED:
		LOG_INF("Reboot application.");
		sys_reboot(SYS_REBOOT_COLD);
		break;

	case ZIGBEE_FOTA_EVT_ERROR:
		LOG_ERR("OTA image transfer failed.");
		break;

	default:
		break;
	}
}
#endif /* CONFIG_ZIGBEE_FOTA */



void initLeds()
{
	led0 = device_get_binding(LED0);
	gpio_pin_configure(led0, LED0_PIN, GPIO_OUTPUT_INACTIVE | LED0_FLAGS);
}



void main(void)
{
	//k_event_init(&m_bootEvent);
	LOG_INF("Starting schnake app for sensor1");

	nrf_power_dcdcen_set(NRF_POWER, 0);
	nrf_power_dcdcen_vddh_set(NRF_POWER, 0);

	m_total_errors = 0;
	m_isConnected = 0;

	zigbee_erase_persistent_storage(false);

#ifdef USE_APP_WATCHDOG
	initWatchdog();
#endif

/*
	zb_set_ed_timeout(ED_AGING_TIMEOUT_64MIN);
	zb_set_keepalive_timeout(ZB_MILLISECONDS_TO_BEACON_INTERVAL(60000));
	zb_zdo_pim_set_long_poll_interval(ZB_MILLISECONDS_TO_BEACON_INTERVAL(60000));
*/
	zb_set_rx_on_when_idle(ZB_FALSE);
#ifdef IEEE_ADDR
	zb_set_long_address(g_ed_addr);
#endif
/*
	zb_sleep_set_threshold(ZB_SCHED_SLEEP_THRESHOLD_MS);
*/

	power_down_unused_ram();
	zigbee_configure_sleepy_behavior(true);
#ifdef CONFIG_ZIGBEE_FOTA
	/* Initialize Zigbee FOTA download service. */
	zigbee_fota_init(ota_evt_handler);

	/* Mark the current firmware as valid. */
	//confirm_image();

	/* Register callback for handling ZCL commands. */
	ZB_ZCL_REGISTER_DEVICE_CB(zigbee_fota_zcl_cb);
#endif /* CONFIG_ZIGBEE_FOTA */

	ZB_AF_REGISTER_DEVICE_CTX(&multi_sensor_ctx);
	sensor_clusters_attr_init();
	initLeds();

#ifdef USE_APP_WATCHDOG
	startWatchdog();
#endif

	zigbee_enable();

	k_timer_start(&bme280_timer, K_SECONDS(60), K_SECONDS(60));
	k_timer_start(&battery_timer, K_SECONDS(90), K_MINUTES(15));

#ifdef CONFIG_ZIGBEE_FOTA
	LOG_INF("Wait for subsystems to say okay");
	k_event_wait_all(&m_bootEvent, EVENT_BATTERY_OKAY | EVENT_SENSOR_OKAY | EVENT_ZIGBEE_OKAY, true, K_FOREVER);

	LOG_INF("All subsystems seems stable, commit boot image");
	/* Mark the current firmware as valid. */
	confirm_image();
#endif
}

static int board_ms88sfe2_nrf52840_init(const struct device *dev)
{
	ARG_UNUSED(dev);

	/* if the nrf52840dongle_nrf52840 board is powered from USB
		* (high voltage mode), GPIO output voltage is set to 1.8 volts by
		* default and that is not enough to turn the green and blue LEDs on.
		* Increase GPIO voltage to 3.0 volts.
		*/
	if((nrf_power_mainregstatus_get(NRF_POWER) == NRF_POWER_MAINREGSTATUS_HIGH) && ((NRF_UICR->REGOUT0 & UICR_REGOUT0_VOUT_Msk) != (UICR_REGOUT0_VOUT_1V8 << UICR_REGOUT0_VOUT_Pos))) {
		NRF_NVMC->CONFIG = NVMC_CONFIG_WEN_Wen << NVMC_CONFIG_WEN_Pos;
		while(NRF_NVMC->READY == NVMC_READY_READY_Busy) {;}

		NRF_UICR->REGOUT0 = (NRF_UICR->REGOUT0 & ~((uint32_t)UICR_REGOUT0_VOUT_Msk)) | (UICR_REGOUT0_VOUT_1V8 << UICR_REGOUT0_VOUT_Pos);

		NRF_NVMC->CONFIG = NVMC_CONFIG_WEN_Ren << NVMC_CONFIG_WEN_Pos;
		while(NRF_NVMC->READY == NVMC_READY_READY_Busy) {;}

		/* a reset is required for changes to take effect */
		NVIC_SystemReset();
	}

	return 0;
}

SYS_INIT(board_ms88sfe2_nrf52840_init, PRE_KERNEL_1, CONFIG_KERNEL_INIT_PRIORITY_DEFAULT);
